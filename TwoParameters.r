#Problem 2
#Highest posterior density region function
hpd<-function(x,dx,p){
  md<-x[dx==max(dx)] #mode
  px<-dx/sum(dx) #normalise density
  pxs<--sort(-px)  ##or sort(px,decreasing=TRUE, so highest value is first)
  ct<-min(pxs[cumsum(pxs)< p]) # find threshold density at which cumulative density exceeds p
  list(hpdr=range(x[px>=ct]),mode=md) } #obtain HPD - range of theta values whose density exceed cutoff

mu.support<-seq(0,2,length=5000)
mu.density<-function(mu,s,y.bar,n){(y.bar^2+s^2/n-2*mu*y.bar+mu^2)^(-n/2)}
mu1.density<-function(mu){mu.density(mu,0.24,1.013,32)}
mu2.density<-function(mu){mu.density(mu,0.2,1.173,36)}
mu3.density<-function(mu){mu.density(mu,0.35,1.281,25)}
p.mu1<-mu1.density(mu.support)
p.mu2<-mu2.density(mu.support)
p.mu3<-mu3.density(mu.support)
ord1<-order(-p.mu1)
ord2<-order(-p.mu2)
ord3<-order(-p.mu3)
mupmu1<-cbind(mu.support[ord1], p.mu1[ord1])
mupmu2<-cbind(mu.support[ord2], p.mu2[ord2])
mupmu3<-cbind(mu.support[ord3], p.mu3[ord3])
plot(mu.support, p.mu1, type="l",
     xlab=expression(mu),ylab=expression(paste(italic("p"),"(mu)")))
hpd(mupmu1[,1],mupmu1[,2],0.95)$hpdr
hpd(mupmu2[,1],mupmu2[,2],0.95)$hpdr
hpd(mupmu3[,1],mupmu3[,2],0.95)$hpdr

#Problem 3
#c
#Contour plots
p.yi2<-function(y,m,s2){pnorm(y+0.5,m,sqrt(s2))-pnorm(y-0.5,m,sqrt(s2))}
yi<-c(10,10,12,11,9)
posterior2<-function(m,s2){
  if(s2<=0){
    0
  }else{
  p<-1
  for(i in 1:5){
  p<-p*p.yi(yi[i],m,s2)
  }
  p
  }
}
posterior1<-function(mu,s2){
  if(s2<=0){
    0
  }else{
  p<-1
  for(i in 1:5){p<-p*dnorm(yi[i],mu,sqrt(s2))
  }
  p
  }
}
gs<-100
mu.range<-seq(8,12.8,length=gs)
s2.range<-seq(0,9,length=gs)
d.posterior2<-matrix(0,gs,gs)
d.posterior1<-matrix(0,gs,gs)
for(i in 1:gs){
   for(j in 1:gs){
     d.posterior2[i,j]<-posterior2(mu.range[i],s2.range[j])
     d.posterior1[i,j]<-posterior1(mu.range[i],s2.range[j])
   }}
grays<- gray((20:0)/20)

image(mu.range,s2.range,d.posterior1,col=grays,xlab = expression(mu),ylab=expression(sigma^2))
box(col="black")

image(mu.range,s2.range,d.posterior2,col=grays,xlab = expression(mu),ylab=expression(sigma^2))
box(col="black")

##Importance Sampling
##to get the mean and variance
install.packages("mvtnorm")
library(MASS)
library(mvtnorm)

S<-10000
sigma.matrix<-matrix(data=c(1,0,0,5),nrow=2)
zSample<-mvrnorm(S,mu=c(10,4),Sigma=sigma.matrix)
impRatio<-c()
for(i in 1:S){
   weight<-posterior1(zSample[i,1],zSample[i,2])/dmvnorm(zSample[i,],mean=c(10,4),sigma=sigma.matrix)
   impRatio<-c(impRatio,weight)
}
s1<-zSample
for(i in 1:S){
   s1[i,]<-zSample[i,]*impRatio[i]
}
aver<-c(mean(s1[,1]),mean(s1[,2]))/mean(impRatio)
aver
#[1] 10.379557  3.816893
#The expectation of posterior1
s2<-zSample
for(i in 1:S){
  s2[i,]<-zSample[i,]^2*impRatio[i]
}
x.squared<-c(mean(s2[,1]),mean(s2[,2]))/mean(impRatio)
var<-x.squared-aver^2
var
#[1] 0.703760 7.688334
#the variance of (mu,sigma^2) in posterior1


impRatio.2<-c()
for(i in 1:S){
  weight<-posterior2(zSample[i,1],zSample[i,2])/dmvnorm(zSample[i,],mean=c(10,4),sigma=sigma.matrix)
  impRatio.2<-c(impRatio.2,weight)
}
s1.2<-zSample
for(i in 1:S){
  s1.2[i,]<-zSample[i,]*impRatio.2[i]
}
aver.2<-c(mean(s1.2[,1]),mean(s1.2[,2]))/mean(impRatio.2)
aver.2
#[1] 10.380168  3.744282
#The expectation of p osterior2
s2.2<-zSample
for(i in 1:S){
  s2.2[i,]<-zSample[i,]^2*impRatio.2[i]
}
x.squared.2<-c(mean(s2.2[,1]),mean(s2.2[,2]))/mean(impRatio.2)
var.2<-x.squared.2-aver.2^2
var.2
#[1] 0.7063472 7.7817080
#the variance of (mu,sigma^2) in posterior2

###failed
#we first need to find the max value of posterior density
mu.seq<-rep(seq(5,15,length=100),100)
s2.seq<-c()
for(i in 1:100){
  s2.seq<-c(rep(i/10,100),s2.seq)}
post.seq<-cbind(mu.seq,s2.seq)
p<-c()
for(i in 1:10000){
   p<-c(p,posterior2(post.seq[i,1],post.seq[i,2]))
   }
max(p)
#max of posterior density is 0.00074783
#let we draw from uniform distribution runif(mu,5,15) and runif(sigma^2,0,10)
M<-0.0008/(1/21)
S<-100
n.draws<-0
draws<-c()
while(n.draws<S){
 x.c<-c(runif(1,9,12),runif(1,0,7))
 accept.prob<-posterior2(x.c[1],x.c[2])/(M*0.0001)
 u<-runif(1,0,1)
 if(accept.prob>=u){
   draws<-rbind(draws,x.c)
   n.draws<-draws+1
  }
}
#doesn't work
#perhaps the accept.prob is too low
 
#Problem 4
py<-function(y){(exp(y)/(1+exp(y)))-exp(y)/(1+exp(y))^2}
plot(seq(-8,8,length=500),py(seq(-8,8,length=500)))

#Problem 5
#a
S<-1000
y<-c()
var<-1/rgamma(S,10,2.5)
for(i in 1:S){
  theta<-rnorm(1,4.1,sqrt(var[i]/20))
  if(i<=0.31*S){
    yi<-rnorm(1,theta,sqrt(var[i]))
  }else if (i<= 0.77*S){
    yi<-rnorm(1,2*theta,2*sqrt(var[i]))
  }else{
    yi<-rnorm(1,3*theta,3*sqrt(var[i]))
  }
  y<-c(y,yi)
}

#b
y.sort<-sort(y) 
y.sort[S*(1-0.75)/2]
y.sort[S*(1+0.75)/2]

#c
dy<-density(y)
py<-dy$y/sum(dy$y)
py.sort<-sort(py,decreasing=T)
ct<-min(py.sort[cumsum(py.sort)< 0.75])
hpdr.x<-dy$x[py>ct]
hpdr<-c(hpdr.x[1])
for(i in 2:length(hpdr.x)){
  if(hpdr.x[i]>hpdr.x[i-1]+0.05){
    hpdr<-c(hpdr,hpdr.x[i-1],hpdr.x[i])
  }
}
hpdr<-c(hpdr,hpdr.x[length(hpdr.x)])

#Problem 6
#a
x<-c(-0.86,-0.3,-0.05,0.73)
n<-c(5,5,5,5)
y<-c(0,1,3,5)
summary(glm(cbind(y,n-y)~x,family = binomial(link=logit)))
#let the approximate distribution be mvnormal
#use the estimates from glm() as mean
#try different sigma matrix
g.mean<-c(0.8466,7.7488)
g.sigma1<-matrix(data = c(1.04,0,0,23.74),nrow=2)#candidate 1
g.sigma2<-matrix(data = c(4,0,0,60),nrow=2)#candidate 2
g.sigma3<-matrix(data = c(8,0,0,120),nrow=2)#candidate 3
g.sigma<-matrix(data = c(8,10,10,120),nrow=2)#candidate 4
logistic<-function(x){1/(1+exp(-x))}
#The unnormalised posterior density 
q.density<-function(a,b){
  result<-1
  for(i in 1:4){
    result<-result*dbinom(y[i],n[i],logistic(a+b*x[i]))
  }
  result  
}
#The density of the normal approximation
g.density<-function(a,b){
  dmvnorm(c(a,b), mean=g.mean,sigma=g.sigma)
}
#find a appropriate normal approximation
find.sigma<-function(m){
  error<-0
  accept<-0
  for(ginger in 1:10000){
    random.n<-mvrnorm(1,mu=g.mean,Sigma=g.sigma)
    rt<-q.density(random.n[1],random.n[2])/(m*g.density(random.n[1],random.n[2]))
    rg<-runif(1,0,1)
    if(rg<rt){
      accept<-accept+1
    }
    if(q.density(random.n[1],random.n[2])>m*g.density(random.n[1],random.n[2])){error<-error+1}
  }   
  list(ac.no=accept,error=error)
  #We want to make "error" lower while keeping accepet.prob greater than 0.1
}

M<-30
#Importance ratio
ratio<-function(a,b){
  q.density(a,b)/(M*g.density(a,b))
}
S<-10000
k<-1000
r<-k
index<-1:10000
s.draws<-mvrnorm(S,mu=g.mean,Sigma=g.sigma)
k.draws<-c()
#sample without replacement
#success!!!
while(r > 0){
  s.index<-sample(index,r,replace=FALSE)
  for(i in s.index){
    u<-runif(1,0,1)
    a.p<-ratio(s.draws[i,1],s.draws[i,2])
    if(u<a.p){#accept this sample
      k.draws<-rbind(k.draws,s.draws[i,])
      index<-index[index!=i]#remove the sampled
    }
  }
  r<-k-length(k.draws)/2
}
plot(k.draws[,1],k.draws[,2],main="scatter plot",sub="sampled without replacement",xlab=expression(alpha),ylab=expression(beta))

#b
S<-10000
k<-1000
r<-k
index<-1:10000
s.draws<-mvrnorm(S,mu=g.mean,Sigma=g.sigma)
k.draws<-c()
#sample with replacement
while(r > 0){
  s.index<-sample(index,r,replace=FALSE)
  for(i in s.index){
    u<-runif(1,0,1)
    a.p<-ratio(s.draws[i,1],s.draws[i,2])
    if(u<a.p){#accept this sample
      k.draws<-rbind(k.draws,s.draws[i,])
    }
  }
  r<-k-length(k.draws)/2
}
plot(k.draws[,1],k.draws[,2],main="scatter plot",sub="sampled with replacement",xlab=expression(alpha),ylab=expression(beta))
