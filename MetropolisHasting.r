install.packages("coda")
library(coda)
#Problem 1

#a

#reading data and formatting
school1<-read.table("school1.dat")
school2<-read.table("school2.dat")
school3<-read.table("school3.dat")
school4<-read.table("school4.dat")
school5<-read.table("school5.dat")
school6<-read.table("school6.dat")
school7<-read.table("school7.dat")
school8<-read.table("school8.dat")
school<-list(school1,school2,school3,school4,school5,school6,school7,school8)
for(i in 1:length(school)){
  school[[i]]<-school[[i]][,1]
}

#The code below uses the algorithms provided in Hoff
#prior parameters
mu0<-7 ; g20<-5
t20<-10; eta0<-2
s20<-15; nu0<-2
#

#starting values
m<-length(school)
n<-sv<-ybar<-rep(NA,m)
for(j in 1:m){
  ybar[j]<-mean(school[[j]])
  sv[j]<-var(school[[j]])
  n[j]<-length(school[[j]])
}
theta<-ybar    ; sigma2<-mean(sv)
mu<-mean(theta); tau2<-var(theta)
#

#setup MCMC
set.seed(1)
S<-5000
THETA<-SMT<-NULL
#

#MCMC algorithm
for(s in 1:S){
  
  #sample new value of the thetas
  for(j in 1:m){
    vtheta<-1/(n[j]/sigma2+1/tau2)
    etheta<-vtheta*(ybar[j]*n[j]/sigma2+mu/tau2)
    theta[j]<-rnorm(1,etheta,sqrt(vtheta))
  }
  
  #sample a new value of sigma2
  nun<-nu0+sum(n)
  ss<-nu0*s20
  for(j in 1:m){ss<-sigma2+sum((school[[j]]-theta[j])^2)}
  sigma2<-1/rgamma(1,nun/2,ss/2)
  
  #sample a new value of mu
  vmu<-1/(m/tau2+1/g20)
  emu<-vmu*(m*mean(theta)/tau2+mu0/g20)
  mu<-rnorm(1,emu,sqrt(vmu))
  
  #sample a new value of tau2
  etam<-eta0+m
  ss<-eta0*t20+sum((theta-mu)^2)
  tau2<-1/rgamma(1,etam/2,ss/2)
  
  #store results
  SMT<-rbind(SMT,c(sigma2,mu,tau2))
  THETA<-rbind(THETA,theta)
}
colnames(SMT)<-c("sigma2","mu","tau2")

#check effective sample size
for(i in 1:m){cat(effectiveSize(THETA[,i]),"\n")}
for(i in 1:3){cat(effectiveSize(SMT[,i]),"\n")}

#b
#posterior mean and 95% CI
apply(SMT,2,mean)
apply(SMT,2,function(x) quantile(x,prob=c(0.025,0.975)))

#prior 95% CI
install.packages("pscl")
library(pscl)
sigma2.quantile.prior<-c(qigamma(0.025,nu0/2,nu0*s20/2),
                         qigamma(0.975,nu0/2,nu0*s20/2))
mu.quantile.prior<-qnorm(c(0.025,0.975),mu0,g20)
tau2.quantile.prior<-c(qigamma(0.025,eta0/2,eta0*t20/2),
                       qigamma(0.975,eta0/2,eta0*t20/2))

#c
R.posterior<-R.prior<-vector(mode = "numeric",length = nrow(SMT))
for(i in 1:length(R.posterior)){
  
  #Calculate R for a MCMC draws
  R.posterior[i]<-SMT[i,3]^2/(SMT[i,3]^2+SMT[i,1]^2)
  
  #Use Monte Carlo simulation to approximate R
  tau2<-1/rgamma(1,eta0/2,eta0*t20/2)
  R.prior[i]<-
}
plot(density(R.posterior),xlab = "R",main ="Posterior density of R")
